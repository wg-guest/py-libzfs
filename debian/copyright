Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: py-libzfs
Source: https://github.com/truenas/py-libzfs
Comment: Build-depends on package in contrib

Files: *
Copyright: 2015      iXsystems, Inc.
           2015-2017 Jakub Klama <jakub.klama@gmail.com>
           2015-2019 William Grzybowski <william@grzy.org>
           2018-2019 Waqar Ahmed <waqarahmedjoyia@live.com>
License: LGPL-3.0-only
Comment: These files were re-licensed from BSD-2-Clause to
         LGPL-3.0-only since Fed 2024.

Files: m4/AX_CHECK_DEFINE.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
License: Special

Files: setup.py
       debian/*
Copyright: 2019 William Grzybowski <william@grzy.org>
           2022 Boyuan Yang <byang@debian.org>
           2024 Shengqi Chen <harry-chen@outlook.com>
License: BSD-2-Clause

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-3.0-only
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3'.

License: Special
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.  This file is offered as-is, without any
 warranty.
